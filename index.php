<?php
class seupdate {
    public function common($params) {
        $marker = '{{ seupdate }}';

        $html = file_get_contents(dirname(__FILE__).'/template/index.html');
        $Cache = new Cache;
        $Cache->lifeTime = 14400;
        if ($Cache->check('pl_seupdate')) {
            $func = $Cache->read('pl_seupdate');
            $func = unserialize($func);
        } else {
            $func = include(dirname(__FILE__).'/func.php');
            if ($func == false) {
                $func = $Cache->read('pl_seupdate');
                $func = unserialize($func);
            } else {
                $Cache->write(serialize($func), 'pl_seupdate', array());
            }
        }

        $Viewer = new Viewer_Manager;
        $html = $Viewer->parseTemplate($html, array('content' => $func));

        $params = str_replace($marker, $html, $params);

        return $params;

    }
}
?>